# Direct dependencies
mkdocs>=1.1
Pygments>=2.4
markdown>=3.2
pymdown-extensions>=7.0
mkdocs-material-extensions>=1.0
mkdocs-awesome-pages-plugin
mkdocs-git-revision-date-localized-plugin
mkdocs-git-committers-plugin-2
mkdocs-print-site-plugin
mkdocs-rss-plugin
mkdocs-nav-enhancements
