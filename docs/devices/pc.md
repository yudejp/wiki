# コンピューター

## 自作
Windows 用デスクトップ

* CPU: AMD Ryzen 5 3600
* RAM: DDR4 8GB * 4 = 32GB
* OS: Windows 10 Pro
* GPU: NVIDIA GeForce GTX 1060 6GB

## iiyama ILeDxi-M012
Linux 用デスクトップ

* CPU: Intel Core i5-6500
* RAM: DDR4 8GB
* OS: Ubuntu Desktop 20.04.2 LTS
* GPU: NVIDIA GeForce GTX 650

## 自作
サーバー  

* CPU: Intel Core i7-2600 @ 3.80GHz (8C)
* RAM: DDR3 8GB * 2 = 16GB
* OS: Ubuntu 20.04.1 LTS x86_64

## HP ENVY x360
ラップトップ、13 インチ パフォーマンスモデル  

* 色: ナイトフォールブラック
* CPU: AMD Ryzen 7 3700U (4C/8T)
* RAM: DDR4 16GB
* GPU: AMD Radeon RX Vega 10
* OS: Windows 10 Home