# 周辺機器

## キーボード
* [ビットフェローズ BFKB113PBK](https://bit-trade-one.co.jp/product/bitferrous/bfkb113pbk/)
* [Majestouch BLACK Tenkeyless 黒軸・テンキーレス・かななし](https://www.diatec.co.jp/products/det.php?prod_c=770)

## マウス
* [Razer DeathAdder Elite](https://www2.razer.com/jp-jp/gaming-mice/razer-deathadder-elite)

## ペンタブレット
* [ワコム Bamboo Fun CTH-470/K1](https://tablet.wacom.co.jp/products/bamboo/bamboofun/)

## プリンター
* [Canon PIXUS TS7330](https://cweb.canon.jp/pixus/lineup/allinone/ts7330/)

## キャプチャーボード
* [SKNET MonsterX Lite SK-MVTG](https://sknet-web.co.jp/sk-mvtg/)

## スピーカー
* Apple iPod Hi-Fi

## マイク
* [Sony ECM-PCV80U](https://www.sony.jp/microphone/products/ECM-PCV80U/)

## アダプター
* [Anker USB-C to HDMI Adapter](https://www.ankerjapan.com/item/A8306.html)
* [Apple Lightning - Digital AVアダプタ](https://www.apple.com/jp/shop/product/MD826AM/A/lightning-digital-av%E3%82%A2%E3%83%80%E3%83%97%E3%82%BF)

## イヤホン, ヘッドフォン
* [final E500](https://snext-final.com/products/detail/E500)
* [Anker Soundcore Vortex](https://www.ankerjapan.com/item/A3031.html)

## バッテリー
* [RAVPower モバイルバッテリー 20000mAh PD対応 60W RP-PB201](https://www.ravpower.jp/rp-pb201)
* [Sony CP-V5BA](https://www.sony.jp/battery/products/CP-V5BA/)

## カメラ
* [GoPro HERO 6 Black](https://gopro.com/ja/jp/)
* [Canon EOS 7D](https://global.canon/ja/c-museum/product/dslr802.html)
* [YONGNUO YN50mm F1.8 単焦点レンズ](https://www.amazon.co.jp/gp/product/B06XKG3M6K/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
